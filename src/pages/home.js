import React from "react";
import Menu from "../components/menu/menu";
import Footer from "../components/footer/footer";
import MainForm from "../components/form/formContainer";
import mainImage from '../images/home-banner.jpg'

export default function Home() {
    return(
       <div className={'home'}>
            <Menu/>
            <img src={mainImage} alt=""/>
            <MainForm/>

            <Footer/>
       </div>


    );
}
