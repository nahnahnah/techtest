import React from "react";
import { StoreProvider, createStore } from 'easy-peasy'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Home from '../../pages/home';
import model from '../../models/model'

const store = createStore(model);

function App() {
  return (
      <StoreProvider store={store}>
    <div className="App">

        <Home/>

    </div>
      </StoreProvider>
  );
}

export default App;
