import React, {Fragment, useEffect, useState } from "react";

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'


 let MainForm = ({
                     options,
                     formSubmitted,
                     respuesta,
                     fetchOptions,
                     postForm,
                     setFormData }) => {

    const [nif, setNif] = useState('');
    const [actividad, setActividad] = useState('');
    const [nifErr, setNifErr] = useState({});
    const [actividadErr, setActividadErr] = useState({});
    useEffect(() => {
        fetchOptions();

        // eslint-disable-next-line
    }, []);

    return (

        <Fragment>

            <Form className={'container'} onSubmit={e => {
                e.preventDefault();
                let elemento = options.find(element => element.description === actividad)
                let code = elemento.code;
                setFormData({
                    nif,
                    code
                });
                postForm({nif,code})
            }}>
                <Form.Group controlId="basicForm">

                    <Form.Control type="nif" maxLength="10" placeholder="NIF" onChange={e => setNif(e.target.value)}/>


                    <Form.Control className={'mt-3'} as="select" onChange={e => setActividad(e.target.value)}>
                        <option>Selector de Actividad</option>
                        {options.map(d => <option key={d.code}>{d.description}</option>)}
                    </Form.Control>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Continuar
                </Button>
            </Form>

            {JSON.stringify(formSubmitted.nif)}

            <div>{JSON.stringify(respuesta)}</div>

        </Fragment>

    );
}

export default MainForm;
