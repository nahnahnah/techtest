import React, { useEffect } from "react";
import { useStoreState, useStoreActions } from "easy-peasy";
import FormComponent from './form';

export default function FormContainer() {
    const options = useStoreState(state => state.options)
    const fetchOptions = useStoreActions(actions => actions.fetchOptions);

    const respuesta = useStoreState(state => state.response)

    const formSubmitted = useStoreState(state => state.formData)

    const postForm = useStoreActions(actions => actions.postForm);

    const setFormData = useStoreActions(actions => actions.setFormData);

    useEffect(() => {
        fetchOptions();

        // eslint-disable-next-line
    }, []);






    return (


        <FormComponent
            options={options}
            formSubmitted={formSubmitted}
            respuesta={respuesta}
            fetchOptions={fetchOptions}
            postForm={postForm}
            setFormData={setFormData}
        />


    );
}
