import React from "react";
import footerImage from '../../images/inbonis-rating.gif'
export default function Footer() {
    return (

        <div className={'customFooter'}>
            <img src={footerImage} alt=""/>

            <hr/>
        </div>

    );
}
