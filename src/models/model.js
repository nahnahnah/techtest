import { action, thunk } from "easy-peasy";
import axios from 'axios';
export default {
    options: [],
    formData: {
        nif: '',
        actividad: ''
    },
    response: null,
    // Thunks
    fetchOptions: thunk(async actions => {
        const res = await fetch(
            "https://demos.inbonis.com/api-coach-es-informa/activities"
        );
        const options = await res.json();

        console.log(options)

        actions.setOptions(options);
    }),
    postForm: thunk(async (actions, payload) => {
        console.log("payload", payload);
        let data = await JSON.stringify({nif: payload.nif, activity_sector: payload.code})
        let config = {
            method: 'post',
            url: 'https://demos.inbonis.com/api-coach-es-informa/diagnosis/anon',
            headers: {
                'Content-Type': 'text/plain'
            },
            data : data
        };
        const res = await axios(config);
        console.log("res ", res)
        const respuesta = await res;

        console.log(respuesta.data)

        actions.setResponse(respuesta);
    }),


    // Actions
    setOptions: action((state, options) => {
        state.options = options;
    }),
    setResponse: action((state, response) => {
        state.response = response;
    }),
    setFormData: action((state, nif, actividad ) => {
        console.log(nif)
        state.formData= {
            nif
        };
        let data = JSON.stringify({nif:nif.nif, activity_sector: nif.code})

        console.log(state.formData)





    }),
}

